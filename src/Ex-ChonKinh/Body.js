import React, { Component } from "react";

export default class Body extends Component {
  data = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./glassesImage/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: "./glassesImage/v2.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: "./glassesImage/v3.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 4,
      price: 70,
      name: "DIOR D6005U",
      url: "./glassesImage/v4.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 5,
      price: 40,
      name: "PRADA P8750",
      url: "./glassesImage/v5.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 6,
      price: 60,
      name: "PRADA P9700",
      url: "./glassesImage/v6.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 7,
      price: 80,
      name: "FENDI F8750",
      url: "./glassesImage/v7.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 8,
      price: 100,
      name: "FENDI F8500",
      url: "./glassesImage/v8.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 9,
      price: 60,
      name: "FENDI F4300",
      url: "./glassesImage/v9.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  ];

  state = {
    glass: this.data[0].url,
    name: this.data[0].name,
    desc: this.data[0].desc,
  };

  renderKinh = () => {
    return this.data.map((item) => {
      return (
        <div onClick={()=>{
          this.handleThayDoiKinh(item.id-1)
        }} className="col-2 pb-4">
          <img style={{ width: "100px" }} src={item.url} alt="" />
        </div>
      );
    });
  };

  handleThayDoiKinh = (index) => {
    this.setState({
      glass: this.data[index].url,
      name: this.data[index].name,
      desc: this.data[index].desc,
    })
  };

  render() {
    return (
      <div
        style={{
          position: "relative",
          paddingTop: "100px",
          background: "url(./glassesImage/background.jpg)",
          height: "100vh",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center center",
          zIndex: "2",
        }}
      >
        <div
          style={{
            position: "absolute",
            backgroundColor: "#00000040",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            zIndex: "-1",
          }}
        ></div>
        <div style={{
            display: "flex",
            justifyContent: "center"
          }}>
          <div style={{
            position: "relative",
            width: "250px",
          }}>
          <img
            style={{ width: "250px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          <div
            style={{
              position: "absolute",
              top: "30%",
              left: "50%",
              transform: "translate(-50%,-30%)",
            }}
          >
            <img style={{ width: "150px" }} src={this.state.glass} alt="" />
          </div>
          <div style={{
              position: "absolute",
              bottom: "0px",
              left: "0px",
              right: "0px",
              height: "auto",
              backgroundColor: "rgb(254 164 0 / 25%)"
            }} ><h4>{this.state.name}</h4>
            <p>{this.state.desc}</p></div>
          </div>
          <div style={{ marginLeft: "30px" }}>
          <img
            style={{ width: "250px" }}
            src="./glassesImage/model.jpg"
            alt=""
          />
          </div>
        </div>
        <div
          style={{
            padding: "40px 20px",
            backgroundColor: "#fff",
            marginTop: "60px",
          }}
          className="container"
        >
          <div className="row">{this.renderKinh()}</div>
        </div>
      </div>
    );
  }
}
