import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div>
        <div
          style={{
            backgroundColor: "rgb(28 26 26 / 80%)",
            padding: "20px 0px",
            position: "absolute",
            width: "100%",
            zIndex: "3",
          }}
        >
          <h2 style={{ margin: "0px", color: "white" }}>
            TRY GLASSES APP ONLINE
          </h2>
        </div>
      </div>
    );
  }
}
